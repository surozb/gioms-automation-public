describe(' Automation', () => {
    it('Swag', () => {
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login')
        cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-input').type('Admin')
        cy.get(':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-input').type('admin123')
        cy.get('.oxd-button').click()
        cy.get(':nth-child(5) > .oxd-main-menu-item').click()
        cy.get('.oxd-autocomplete-text-input input').type('char').wait(2000)
        cy.get('.oxd-autocomplete-option:first-child').click()

     
        cy.get('.oxd-date-input input[placeholder="From"]').type('2023-12-25')
        cy.get('.oxd-date-input input[placeholder="To"]').type('2023-12-27')

        cy.get('.oxd-select-text-input[tabindex=0]').click()
    })
})
