describe('Local Scope in Cypress', () => {
    it('demonstrates local scope within a function', () => {
      let localVar = 'Local Variable';
      const PI = 3.14;
  
      cy.wrap(localVar).should('eq', 'Local Variable'); // Accessible within the test
  
      // Attempting to access the variables outside this block would result in an error.
      // cy.log(localVar);
      // cy.log(PI);
    });
    cy.log(PI);
  });
  