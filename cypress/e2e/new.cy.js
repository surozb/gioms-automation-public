// describe(' Automation', () => {
//     it('Swag', () => {
//         cy.visit('https://www.saucedemo.com',{ failOnStatusCode: false })
//         cy.get('#user-name').type('standard_user')
//         cy.get('#password').type('secret_sauce')
//         // cy.get('#login-button').click()
//         // cy.get('.bm-burger-button').click()
//         // cy.get('#logout_sidebar_link').click()


//     })
// })

const login = () => {
    cy.visit('https://www.saucedemo.com', { failOnStatusCode: false })


}

describe('UI', () => {
    // beforeEach(login);
    beforeEach(() => {
        login();
        cy.clearAllCookies();
        cy.clearCookies();
        cy.clearLocalStorage();
        cy.clearAllLocalStorage();
        cy.clearAllSessionStorage();
        // Clear all sessions saved on the backend, including cached global sessions.
        Cypress.session.clearAllSavedSessions()
        // Clear all storage and cookie date across all origins associated with the current session.
        Cypress.session.clearCurrentSessionData()
        // Get all storage and cookie data across all origins associated with the current session.
        // Get all storage and cookie data saved on the backend associated with the provided session id.
    });

    it("app", () => {
        cy.get('#user-name').type('standard_user')
        cy.get('#password').type('secret_sauce')
        cy.get('#login-button').click()
        cy.get('.bm-burger-button').click()
        cy.get('#logout_sidebar_link').click()
    })

});