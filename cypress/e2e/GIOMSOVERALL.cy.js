describe('Gioms Automation', () => {

    beforeEach('Main Landing page Login', () => {

        cy.visit('/')

    })

    it('Request Leave for employee', () => {
        cy.get('.appbar > div > div > ul.list-app > li > a[title = "E-attendance"]').click()
        // leave request 
        cy.contains('My Details').click()
        cy.get('a[href="#/e-attendance/my-leave"]').click()
        cy.wait(3000)
        cy.get('[class ="btn btn-primary btn-icon lft ml-3"]').click()

        cy.get('#isApprover').click()
        cy.get('#approver').click()
        cy.get('.css-1qje3pp').contains('Prem Sharan Shrestha | Director General').click()
        // Click to open the dropdown
        cy.get('#leaveSetupId').click();
        cy.get('.css-zqovoj-menu > .css-1qje3pp').children('div')
            .first().next().next().next()// Select the specific element directly
            .should('have.text', 'Sick leave') // Ensure it contains the expected text list of leave name 
            .click() //(1.Casual Leave, 2.Festival Leave, 3.Home Leave, 4.Sick leave, 5.Paternity leave,6.Obsequies Leave,7.Study leave,8.Study leave GVT)
        // Date Selection
        cy.get('#fromDateNp').click()
        cy.get('.month-day').not('.disabled').contains('५').click()
        cy.get('#toDateNp').click()
        cy.get('.month-day').not('disabled').contains('१५').click()
        // Document Upload
        cy.get('input[type=file]').selectFile('cypress/fixtures/darta sample.pdf', { force: true })
        cy.get('.form-group .form-control').type('This is my leave')
        cy.wait(2000)
        // Confirmation process
        cy.get('[title=""] > .btn').click()
        cy.get('.text-end > .btn').click()
        cy.wait(2000)
        cy.get('.modal-footer > .btn-success').click()
        cy.wait(2000)
        cy.get(':nth-child(8) > :nth-child(1) > .modal > .modal-dialog > .modal-content > .modal-footer > .btn-success').click()
    })

 
    // Negative testing and assertion






    it('Register Darta Flow by Person A', () => {

        cy.get('input[name=username]').as('username')
        cy.get('input[name=password]').as('password')

        cy.get('@username').type('248065')
        cy.get('@password').type('Admin@123')
        cy.get('button[type=submit]').click()
        cy.wait(3000)
        cy.get('.lang').click()



        cy.get('.appbar > div > div > ul.list-app > li > a[title = "Darta Chalani"]').click()
        cy.wait(3000)
        cy.get('.sidebar > [style="position: relative; overflow: hidden; width: 100%; height: 100%;"] > [style="position: absolute; inset: 0px; overflow: scroll; margin-right: -15px; margin-bottom: -15px;"] > .list > :nth-child(2)').click()
        cy.get('[href="#/darta-chalani/darta/list?list=registered"]').click()
        cy.wait(2000)
        cy.get('.btn-primary').click()
        cy.get('#senderName').type('Person 1')
        cy.get('input[name="senderAddress"]').type('sanepa')
        cy.get('input[name="senderEmail"]').type('person1@yopamil.com')
        cy.get('input[name="senderPhoneNo"]').type('9840000000')
        cy.get(':nth-child(6) > .form-group > .form-control').type('1')
        cy.get('#dispatchDateNp').click()
        cy.get('.today').click()
        cy.get('input[name="subject"]').type('This sbuject is for darta sample of person 1')
        cy.get('#document').selectFile('cypress/fixtures/darta sample.pdf', { force: true })
        cy.get('.text-end > .btn-success > div > span').click()
    })

    it.only('Darta received by office head role and forward to respective section', () => {
        cy.get('input[name=username]').as('username')
        cy.get('input[name=password]').as('password')

        cy.get('@username').type('142037')
        cy.get('@password').type('Admin@123')
        cy.get('button[type=submit]').click()
        cy.wait(3000)
        cy.get('.lang').click()

        cy.get('.appbar > div > div > ul.list-app > li > a[title = "Darta Chalani"]').click()
        cy.wait(3000)
        cy.get('.sidebar > div > div > .list > :nth-child(2)').click()
        cy.get('[href="#/darta-chalani/received-letter?list=inbox"]').click()
        cy.wait(2000)
        
        cy.get('.tr > .td > .table-action > li > button[type="submit"] > i')
        .eq(1)
        .should('have.class','ic-view').click()
        cy.wait(3000)

        //forward button
        cy.get('.btn-success')
        .should('have.text',' Forward').click()

        //selection of section
        cy.get('#selectedUnit > .css-yfjqgd > .css-xqn7vz').click()
        cy.get('#react-select-2-option-8 > .d-flex')
        .should('have.text','Cyber Forensic Unit').click()
        cy.get('. css-xqn7vz').click()
        cy.get('. css-zqovoj-menu > . css-1qje3pp > . css-85qtxc-option')
        .should('have.text','Pradip Gautam | Computer Engineer').click()

        cy.get('#description').type('Important Work')
        cy.get('.btn > div > span').click()

    })

    // it('Organization Profiling', () => {
    //     cy.get('.appbar > div > div > ul.list-app > li > a[title = "Organization Profiling"]').click()
    // })


    // it('User Management', () => {
    //     cy.get('.appbar > div > div > ul.list-app > li > a[title = "User Management"]').click()
    // })
})  